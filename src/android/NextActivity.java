package com.enparadigm.smartsellplugin;

import android.app.Activity;
import android.os.Bundle;

import com.enparadigm.smartsell.R;

public class NextActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);
    }
}
