package com.enparadigm.smartsellplugin;

import android.content.Intent;
import android.widget.Toast;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * This class echoes a string called from JavaScript.
 */
public class SmartSellPlugin extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("initialize")) {
            Toast.makeText(cordova.getContext(), "initialize", Toast.LENGTH_SHORT).show();
            return true;
        } else if (action.equals("startActivity")) {
            Intent intent = new Intent(cordova.getContext(), NextActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            cordova.getContext().startActivity(intent);
            return true;
        }
        return false;
    }
}
