var exec = cordova.require('cordova/exec');
var PLUGIN_NAME = "SmartSellPlugin";
var SmartSellPlugin = function() {
    console.log('SmartSellPlugin instanced');
};

SmartSellPlugin.prototype.initialize = function(onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'initialize', []);
};

SmartSellPlugin.prototype.startActivity = function(onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'startActivity', []);
};

if (typeof module != 'undefined' && module.exports) {
    module.exports = SmartSellPlugin;
}